import { Selector } from 'testcafe';
export const inputCode = Selector('input').withAttribute('id', 'accessCodeInput');
export const buttonLogin = Selector('button').withAttribute('id', 'accessCode_valid');
export const pseudo = Selector('input').withAttribute('id', 'nickname');
export const buttonPseudoJoin = Selector('button[type=submit]');
export const stormTitle = Selector('strong').withAttribute('class', 'tf-l');
export const livestormTitle = Selector('p').withAttribute('class', 'klax-carousel-item__question f-large b u-break-word');
export const stormIdeas = Selector('div').withAttribute('class', 'dib v-mid button ba b--gray br2 pr2');
export const ideasTitle = Selector('p').withAttribute('class', 'f-huge-r u-break-word');
export const ideasList = Selector('ul').withAttribute('class', 'tl');


