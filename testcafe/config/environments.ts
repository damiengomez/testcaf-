export const environments: IEnvInfo[] = [
  { name: 'recette', url: 'https://rec-pichet.crm4.dynamics.com/main.aspx' },
  { name: 'int', url: 'https://int-m-pichet.crm4.dynamics.com/main.aspx' },
  { name: 'pprod', url: 'https://pprod-pichet.crm4.dynamics.com/main.aspx' },
];

const envNames: string[] = environments.map((e) => e.name);

export function env(name: TargetEnvironnement | undefined): IEnvInfo | undefined {
  if (name === undefined) {
    // tslint:disable-next-line:no-console
    console.warn(`Environment name is undefined. Available environments are: ${envNames}.
      (You can optionnaly add to the testcafe command-line the option: --env=${envNames[0]})`);
    return undefined;
  }
  const foundEnvironment = environments.filter((e) => e.name === name)[0];
  if (foundEnvironment) {
    return foundEnvironment;
  }

  // tslint:disable-next-line:no-console
  console.warn(`Environment "${name}" is not found. Available environments are: ${envNames}`);
  return undefined;
}

export interface IEnvInfo {
  name: TargetEnvironnement;
  url: string;
}
export type TargetEnvironnement = 'any' | 'recette' | 'int' | 'pprod';
