import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import * as selector from '../selectors';

/**
 * @step
 * @then,@when("I see expected words")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);


  await t
    .setTestSpeed(config.testSpeed)
    .expect(selector.ideasList.textContent).contains('Étoiles')
    .expect(selector.ideasList.textContent).contains('Année')
    .expect(selector.ideasList.textContent).contains('Surprise')
    .expect(selector.ideasList.textContent).contains('Encore')
    .expect(selector.ideasList.textContent).contains('Difficile')
    .expect(selector.ideasList.textContent).contains('Avoir_plus_de_temps')
    .expect(selector.ideasList.textContent).contains('Jamais')
    .expect(selector.ideasList.textContent).contains('absent');
};
