import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import * as selector from '../selectors';

/**
 * @step
 * @given,@when,@and("I send a pseudo")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);

  const randomPseudo = makeid();

  await t
    .setTestSpeed(config.testSpeed)
    .typeText(selector.pseudo, 'Pierre ' + randomPseudo)
    .click(selector.buttonPseudoJoin);
};

function makeid() {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < 6; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};