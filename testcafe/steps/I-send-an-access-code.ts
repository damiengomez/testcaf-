import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import * as selector from '../selectors';

/**
 * @step
 * @given,@when("I send an access code")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);

  await t
    .setTestSpeed(config.testSpeed)
    .typeText(selector.inputCode, 'VHVSWP')
    .click(selector.buttonLogin);
};
