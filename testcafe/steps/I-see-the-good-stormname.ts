import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import * as selector from '../selectors';

/**
 * @step
 * @then,@when("I see the good stormname")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);

  await t
    .setTestSpeed(config.testSpeed)
    .expect(selector.stormTitle.textContent).contains("Un mot pour l'année écoulée? ");
};
